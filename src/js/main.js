$(document).ready(function(){

    //плавная прокрутка до якоря
    $('a.soft[href^="#"]').click(function (event) {
        event.preventDefault();
        var el = $(this).attr('href');
        $('html, body').animate({ scrollTop: $(el).offset().top-100}, 500); //offset to menu

        if($('.navbar-collapse').hasClass('xs-opened')){navtoggle();}

        // return false;
    });

    // всплывающие блоки
    $(".shows").on('click', function () {
        id = $(this).data('show');
        // console.log(id);
        if($('#'+id).css('display') == 'none'){
            $('#'+id).show(300);
            $(this).children('.glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            $(this).children('.icons-arrow-gray').addClass('top');
        }else{
            $('#'+id).hide(300);
            $(this).children('.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $(this).children('.icons-arrow-gray').removeClass('top');
        }
    });

    //меню для мобильной версии
    $('.navbar-toggle').click(function(){
        navtoggle();
    });

    function navtoggle() {
        $('.navbar-toggle').toggleClass('collapsed');
        $('.navbar-collapse').toggleClass('xs-opened');
    }

    // contactModal
    $('.modalopen').click(function () {
        $('#contactModal').show();
        if($(this).data('text')=='iis'){
            $('#myModalLabel').html('Связаться с финансовым советником');
        }
        else if($(this).data('text')=='question'){
            $('#myModalLabel').html('Задать вопрос специалисту');
        }
        else if($(this).data('text')=='openiis'){
            $('#myModalLabel').html('Открыть ИИС');
        }
        $('#myModalDescription').html('');
        return false;
    });
    
    $('.modalclose').click(function () {
        $('#contactModal').hide();
    });

        //slick
    $('.js-slider').slick({
        dots: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: 'ease-in',
        infinite: true
    });

    function calculateSave() {
        var obj = {user:'', userPrice:0, counts:0, times:0, summ:0};

        var price = {
            type1:[220, 200, 187.5, 175],
            type2:[200,  187.5, 175, 162.5],
            type3:[187.5, 175, 162.5, 150]
        }

        return function (result) {
            if(result) return obj;
            $('.js-calc .js-descr').removeClass('error');
            if(parseInt($('.js-calc .control.c3').val()) < 8){
                $('.js-calc .js-descr').addClass('error');
                $('.js-calc .js-sum').html(0);
                return;
            }

            var inputs = {
                user : $('.js-calc .control.c1 option:selected').text() || obj.user,
                userPrice :  obj.userPrice,
                counts : Math.abs($('.js-calc .control.c2').val()),
                times : parseInt($('.js-calc .control.c3').val()),
                summ : obj.summ
            };

            var typeUser = $('.js-calc .control.c1').val();

            var typePrice;
            if(inputs.counts == 1) typePrice = 0;
            else if(inputs.counts > 1 && inputs.counts <6) typePrice = 1;
            else if(inputs.counts > 5 && inputs.counts <11) typePrice = 2;
            else if(inputs.counts > 10 ) typePrice = 3;

            inputs.userPrice = price[typeUser][typePrice] || 0;
            inputs.summ = inputs.userPrice * inputs.counts * inputs.times;
            obj = inputs;
            $('.js-calc .js-sum').html(inputs.summ);
        }
    };
    var calc = calculateSave();

    $('.js-phone-mask').mask('+9 (999) 999-99-99');


    $('.js-calc .control').change(function () {
        calc();
    });

    $('.js-form .control').change(function () {
        if($(this).val() != ''){
            $(this).addClass('blue')
        }
    });

    function calcToText() {
        var qcalc = calc(true);
        var qtext = "Выбрано <b>" + qcalc.counts +
            "</b>  <b>" +qcalc.user+ "</b>" +
            " на <b>" + qcalc.times + "</b> часов<br>" +
            "Расчитанная сумма = <b>" +  qcalc.summ + "</b>";
        return qtext;
    }

    // ajax отправка письма
    $(".feedbackForm").submit(function(e){
        e.preventDefault();
        $('.js-results').html('');
        var form = $(this);
        var error = false;
        form.find('input.r').each( function(){
            if ($(this).val() == '') {
                $('.js-results').html('Зaпoлнитe пoлe "'+$(this).data('name')+'"!');
                error = true;
            }
        });
        if (!error) {
            var data = form.serializeArray();
            data.push({name: 'link', value: window.location.href});
            data.push({name: 'description', value: calcToText()});
            $.ajax({
                type: 'POST',
                url: 'send.php',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success:function (data) {
                    if(data =='success') {

                        $(".feedbackForm").hide(500);
                        $('.js-results').html('<h3>Заявка отправлена!</h3><p>наш менеджер скоро свяжется с вами</p>')
                    }
                },
                complete: function(data) {}

            });
        }
        return false;
    });




});



