$(document).ready(function(){

    // menu fix
    var menu = $("#top-menu");
    var header = $('.main-top-line');

    $(window).scroll(function(){
        var height = header.height();
        if ( $(this).scrollTop() > height && menu.hasClass("relative") ){
            menu.removeClass("relative").addClass("fixed");
        } else if($(this).scrollTop() <= height && menu.hasClass("fixed")) {
            menu.removeClass("fixed").addClass("relative");
        }
    });

    //плавная прокрутка до якоря
    $('a.soft[href^="#"]').click(function (event) {
        event.preventDefault();
        var el = $(this).attr('href');
        $('html, body').animate({ scrollTop: $(el).offset().top-100}, 500); //offset to menu

        if($('.navbar-collapse').hasClass('xs-opened')){navtoggle();}

        // return false;
    });

    // всплывающие блоки
    $(".shows").on('click', function () {
        id = $(this).data('show');
        // console.log(id);
        if($('#'+id).css('display') == 'none'){
            $('#'+id).show(300);
            $(this).children('.glyphicon').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            $(this).children('.icons-arrow-gray').addClass('top');
        }else{
            $('#'+id).hide(300);
            $(this).children('.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $(this).children('.icons-arrow-gray').removeClass('top');
        }
    });

    //меню для мобильной версии
    $('.navbar-toggle').click(function(){
        navtoggle();
    });

    function navtoggle() {
        $('.navbar-toggle').toggleClass('collapsed');
        $('.navbar-collapse').toggleClass('xs-opened');
    }

    var menuItems = $('.nav > li > .nav-arrow');
    for (var i = 0; i < menuItems.length; i++) {
        $(menuItems[i]).click( function() {
            if($(this).parent().hasClass('mobile-open')) {
                $(this).parent().removeClass('mobile-open');
                return;
            }
            for (var i = 0; i < menuItems.length; i++) {
                $(menuItems[i]).parent().removeClass('mobile-open');
            }

            $(this).parent().addClass('mobile-open');
        });
    }

    // contactModal
    $('.js-modalOpen').click(function () {
        $('#contactModal').show();
        return false;
    });
    $('.js-modalClose').click(function () {
        $('#contactModal').hide();
    });


        //slick
    $('.js-slider').slick({
        dots: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: 'ease-in',
        infinite: true
    });

    $('.js-header-slick').slick({
        dots: false,
        speed: 1000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: 'ease-in',
        infinite: true,
        centerMode: true,
        responsive : [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 3
            }
        },{
            breakpoint: 900,
            settings: {
                slidesToShow: 2
            }
        },{
            breakpoint: 500,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $('.js-quotations-slick').slick({
        dots: false,
        speed: 1000,
        arrows: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        cssEase: 'ease-in',
        infinite: true,
        responsive : [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 3
            }
        },{
            breakpoint: 900,
            settings: {
                slidesToShow: 2
            }
        },{
            breakpoint: 500,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    //блок вопросов
    // $('.answers-block .item').click(function (event) {
    //     $(this).toggleClass('active');
    // });

    var answersItems = $('.answers-block .item');
    for (var i = 0; i < answersItems.length; i++) {
        $(answersItems[i]).click( function() {
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                return;
            }
            for (var i = 0; i < answersItems.length; i++) {
                $(answersItems[i]).removeClass('active');
            }

            $(this).addClass('active');
        });
    }

});


