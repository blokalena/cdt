$(document).ready(function(){



    var chartArea = document.getElementById('chartArea').getContext('2d');
    new Chart(chartArea, {
        type: 'doughnut',
        data: {
            labels: ["Параметр 1", "Параметр 2", "Параметр 3"],
            datasets: [
                {
                    backgroundColor: ["#3e95cd","#cccccc", "#da2d29" ],
                    data: [52,16,31]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'График по 223 ФЗ'
            },
            legends:{
                position:'right'
            }
        }
    });



    var chartLine = document.getElementById('chartLine').getContext('2d');
    new Chart(chartLine, {
        type: 'line',
        data: {
            labels: ['Янв 1','Янв 7','Янв 14','Янв 21','Янв 28','Фев 4','Фев 11','Фев 18','Фев 25'],
            datasets: [{
                data: [250,500,380,500,700,300,190,250,690],
                label: "2017",
                borderColor: "#3e95cd",
                backgroundColor: '#4FB6FF',
                fill: false
            }, {
                data: [110,220,250,150,500,650,400,350,300],
                label: "2018",
                borderColor: "#da2d29",
                backgroundColor: '#FF3921',
                fill: false
            }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'График по банкротству'
            }
        }
    });

});



