<!DOCTYPE HTML>

<html>
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<![endif]-->
	<title>Your Website</title>
    <link rel="stylesheet" href="public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="public/css/index.css?04" type="text/css" />

    <script src="public/js/ie/jquery.placeholder.min.js"></script>
    <script src="public/js/ie/html5shiv.js"></script>
    <script src="public/js/ie/respond.min.js"></script>
    <script type="text/javascript" src="public/js/ie/background_size_emu.js"></script>
</head>

<body>

	<header>
        <div class="main-top-line">
            <div class="container2">
                <div class="row">
                    <div class="main-top-line_col-scroll">
                        <div id="marquee">
                            <a href="page/about.php">
                            Ускоренная регистрация на ЭТП в течение 3 часов!
                            </a>
                        </div>
                    </div>
                    <div class="main-top-line_col-info right">
                        <div class="phone-block">
                            <i class="ico phone"></i>
                            <span><a href="tel:+78126464818">+7 (812) 64-64-818</a></span>
                        </div>
                        <div class="social-block social-color-bw">

                            <i class="ico social vk"></i>
                            <i class="ico social fb"></i>
                            <i class="ico social ub"></i>
                            <i class="ico social inst"></i>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div>
            <?php include("page/part/menu.php")?>
        </div>

	</header>
	
	<div class="main-page-blocks">

        <div class="row main_blocks_first">
            <div class="container2">
                <div class="container">
                    <div id="art_1" class="col-sm-6 article">
                        <div class="art_1_content">
                            <p class="h1">Арбитражным управляющим</p>
                            <ul class="underline">
                                <li><span>Подбор тарифа для проведения торгов</span></li>
                                <li><span>Организация торгов “под ключ”</span></li>
                                <li><span>Составление положения о торгах</span></li>
                                <li><span>Оптимизация сообщения в Коммерсантъ</span></li>
                                <li><span>Спец.счёт для приёма задатков</span></li>
                            </ul>
                            <a class="btn btn-arrow">Все услуги</a>
                        </div>
                    </div>

                    <div id="art_2" class="col-sm-6 article">
                        <div class="content">
                            <p class="h1">Покупателям <span class="un-upp">на банкротных торгах</span></p>
                            <ul class="underline">
                                <li><span>Ускоренная регистрация на ЭТП за 3 часа</span></li>
                                <li><span>Агентские услуги на торгах</span></li>
                                <li><span>Помощь в получении ЭЦП</span></li>
                                <li><span>Удалённая подача заявки на участие в торгах</span></li>
                                <li><span>Удалённая подача заявки на регистрацию</span></li>
                            </ul>
                            <a class="btn btn-arrow">УСЛУГИ ДЛЯ ПОКУПАТЕЛЕЙ</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row main_blocks_second">
            <div class="container2">
                <div class="container">
                    <div id="art_3" class=" col-sm-6 dark article">
                        <div class="content-sm">
                            <p class="h1">Закупки</p>
                            <p class="h2">Организаторам</p>
                            <ul class="underline">
                                <li><span>Организация торгов по 223 ФЗ</span></li>
                                <li><span>Организация торгов для коммерческой компании</span></li>
                                <li><span>Разработка внутренних документов и положений</span></li>
                                <li><span>Развёртывание корпоративной ЭТП</span></li>
                            </ul>
                            <p class="h2">Поставщикам</p>
                            <ul class="underline">
                                <li><span>Мониторинг торгов</span></li>
                                <li><span>Получение ЭЦП</span></li>
                                <li><span>Банковская гарантия и тендерный кредит</span></li>
                            </ul>
                            <a class="btn btn-arrow">Все услуги </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="art_4" class="dark article">
                            <div class="content">
                                <p class="h1">Реализация арестованного имущества</p>
                                <p class="mainpage-text">
                                    Информация о проведении торгов по реализации имущества должников и продаже заложенного имущества размещается в соответствии с Гражданским кодексом Российской Федерации, Федеральным законом от 2 октября 2007 г. № 229-ФЗ «Об исполнительном производстве», Федеральным законом от 16 июля 1998 г. № 102-ФЗ «Об ипотеке (залоге недвижимости)»
                                </p>
                                <a class="btn btn-arrow">перейти к торгам </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row main_blocks_third">
            <div class="container2">
                <div class="container">
                    <div id="art_6"  class="col-sm-8 dark article">
                        <div class="content-sm">
                            <p class="h1">Агентские услуги на торгах</p>
                            <!--p class="mainpage-text">Электронная цифровая подпись</p-->
                            <a class="btn btn-arrow">Подробнее</a>
                        </div>
                    </div>

                    <div id="art_7" class="col-sm-4 dark article">
                        <div class="content">
                            <p class="h1">Получить ЭЦП</p>
                            <p class="mainpage-text">Электронная цифровая подпись</p>
                            <a class="btn btn-arrow">ПОЛУЧИТЬ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row main_blocks_fourth">
            <div class="container2">
                <div class="row">
                    <div id="art_8" class="news-blocks article">
                        <div class="row">
                            <div class="article news-block_1">
                                <div class="news-block_img img_1">
                                    <div class="hover"><span>Подробнее</span><i></i></div>
                                </div>
                                <div class="news-block_description content-sm">
                                    <p class="news-block_type">Статья</p>
                                    <p class="news-block_date">29 январь 2018</p>
                                    <p class="news-block_title">Владимир Тарасов: “Просто о сложном”</p>
                                </div>
                            </div>
                            <div class="article news-block_2">
                                <div class="news-block_img img_2">
                                    <div class="hover"><span>Подробнее</span><i></i></div>
                                </div>
                                <div class="news-block_description content-sm">
                                    <p class="news-block_type">Статья</p>
                                    <p class="news-block_date">29 январь 2018</p>
                                    <p class="news-block_title">Владимир Тарасов: “Просто о сложном”</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="art_9" class="dark article">
                        <div class="content">
                            <div class="news-block_description">
                                <p class="news-block_type">Событие</p>
                                <p class="news-block_date">29 январь 2018</p>
                                <p class="news-block_title">Бесплатный семинар
                                    персональное управленческое искусство</p>
                                <a class="btn btn-arrow">принять участие</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mp-bottom-block">
            <div class="row">

                <p class="h1">Реализация арестованного имущества</p>
<!--                <div class="control-menu content-sm" >-->
<!--                    <span class="active">Оргтехника</span>-->
<!--                    <span>Автомобили</span>-->
<!--                    <span>Недвижимость</span>-->
<!--                    <span>Производственные станки</span>-->
<!--                </div>-->

            </div>
            <div class="row">
                <div class="row quotations js-quotations-slick container2">
                    <a href="#" class="quotations_block">
                        <p class="quotations_price falling"><span>16 113 387 руб.</span></p>
                        <p class="quotations_name">Сетевое оборудование ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price growth"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price falling"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price falling"><span>16 113 387 руб.</span></p>
                        <p class="quotations_name">Сетевое оборудование ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price growth"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price growth"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price falling"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price falling"><span>16 113 387 руб.</span></p>
                        <p class="quotations_name">Сетевое оборудование ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price growth"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                    <a href="#" class="quotations_block">
                        <p class="quotations_price growth"><span>62 000 000 руб.</span></p>
                        <p class="quotations_name">«Самонесущие защищенные и изолированные провода» (ГКПЗ ...</p>
                    </a>
                </div>
            </div>
        </div>

	</div>

    <?php include("page/part/footer.php")?>

    <script src="public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="public/js/slick.min.js" type="text/javascript"></script>
    <script src="public/js/main.js?01" type="text/javascript"></script>
    <script src="public/js/scrollLine.js" type="text/javascript"></script>

    <script>
        $('.menu-main').addClass('active');
        $('#top-menu').removeClass('fixed').addClass('relative');
    </script>

</body>

</html>
