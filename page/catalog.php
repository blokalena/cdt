<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<![endif]-->
	<title>Тарифы и услуги</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?01" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>

    <!--    <link rel="stylesheet" href="public/css/main.min.css?04" type="text/css" />-->
</head>

<body>

	<header>
            <?php include("../page/part/menu.php")?>
	</header>
	
	<div class="catalog-blocks">

        <div class="catalog-slider-top">
            <div class="container2">
                <div class="offset-left-menu">
                    <div class="breadcrumbs breadcrumbs_white">
                        <a href="/">Главная</a>
                        <span>Тарифы и услуги</span>
                    </div>

                    <h1 class="left">Тарифы и услуги</h1>
                    <div>
                        <ul class="tab-menu">
                            <li onclick="$('#text1').show();$('#text2').show();$('#text3').show();$('#text4').show();$('#text5').show();$('#text6').show();$('.tab-menu .active').removeClass('active');$(this).addClass('active')" class="active"><a>Все</a></li>
                            <li onclick="$('#text1').show();$('#text2').show();$('#text3').hide();$('#text4').hide();$('#text5').hide();$('#text6').hide();$('.tab-menu .active').removeClass('active');$(this).addClass('active')"><a>Покупателю</a></li>
                            <li onclick="$('#text1').hide();$('#text2').hide();$('#text3').show();$('#text4').show();$('#text5').hide();$('#text6').hide();$('.tab-menu .active').removeClass('active');$(this).addClass('active')"><a>Организатору торгов</a></li>
                            <li onclick="$('#text1').hide();$('#text2').hide();$('#text3').hide();$('#text4').hide();$('#text5').show();$('#text6').show();$('.tab-menu .active').removeClass('active');$(this).addClass('active')"><a>Закупки по 223 ФЗ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container2">
            <h2 class="underline offset-sm" id='text1'>Покупателю</h2>

            <div class="row" id='text2'>
                <div class="catalog_preview_2_block catalog_preview_2_block_new catalog_preview_color_0 box-shadow article dark">
                    <div class="title_new">Помошь в получении ЭЦП</div>
                    <a href="#" class="btn btn-arrow">Подробнее </a>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Ускоренная регистрация (Экспертиза)</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Ускоренная регистрация (Экспертиза+)</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Помощь в получении ЭЦП</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Удалённая настройка компьютера</p>
                    </div>
                </div>

                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Удалённая подача заявки на участие в торгах</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Услуги Агента на торгах</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Удалённая подача заявки на регистрацию</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Оценка имущества</p>
                    </div>
                </div>
                <div class="catalog_preview_2_block catalog_preview_2_block_new box-shadow catalog_preview_color_1 article dark">
                    <div class="title_new">Комплексное сопровождение на торгах</div>
                    <a href="#" class="btn btn-arrow">Подробнее </a>
                </div>

            </div>

            <h2 class="underline offset-sm" id='text3'>Организатору торгов</h2>


            <div class="row" id='text4'>
                <div class="catalog_preview_2_block catalog_preview_2_block_new catalog_preview_color_0 box-shadow article dark">
                    <div class="title_new">Подбор тарифа для проведения торгов на ЭТП</div>
                    <a href="#" class="btn btn-arrow">Подробнее </a>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Помощь в регистрации на ЭТП</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Предоставление спец счёта для приёма задатков</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Составление Положения о порядке продажи имущества</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Оптимизация текстов и помощь в размещении сообщений о торгах в СМИ</p>
                    </div>
                </div>

                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Публикация новости на сайте ЦДТ</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Организация торгов</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Удалённая настройка компьютера</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Оценка имущества</p>
                    </div>
                </div>
                <div class="catalog_preview_2_block catalog_preview_2_block_new box-shadow catalog_preview_color_1 article dark">
                    <div class="title_new">Помошь в получении ЭЦП</div>
                    <a href="#" class="btn btn-arrow">Подробнее </a>
                </div>

            </div>

            <h2 class="underline  offset-sm" id='text5'>Закупки по 223 ФЗ</h2>

            <div class="row" id='text6'>
                <div class="catalog_preview_2_block catalog_preview_2_block_new catalog_preview_color_1 box-shadow article dark">
                    <div class="title_new">Проведение закупок <br>"Под ключ"</div>
                    <a href="#" class="btn btn-arrow">Подробнее </a>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Разработка и/или оптимизация положения о закупках</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Помощь в составления плана закупок</p>
                    </div>

                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Анализ технического задания на предмет соответствия законодательству</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Оптимизация и доработка ТЗ под требования заказчика</p>
                    </div>
                </div>

                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Составление пакета документов для проведения закупок</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Организационно-техническое сопровождение работы в ЕИС</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Настройка рабочего места для работы с порталом закупок и ЭТП</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Публикация закупки на ЭТП</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Анализ поступивших заявок по итогам закупки на соотвествие требованиям документации</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Подготовка проекта протокола заседания комиссии</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Публикация протокола на ЭТП</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Помощь в публикации информации в реестре договоров</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Помощь в составлении и публикации отчёта по договорам</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Предоставление интересов в территориальных ФАС России</p>
                    </div>
                </div>
                <div class="catalog_preview_1_block box-shadow">
                    <div class="img img-1"></div>
                    <div class="descr">
                        <p>Оказание устных и письменных консультаций</p>
                    </div>
                </div>

            </div>

        </div>

        <div class="form_block">
            <?php include("part/contacts-form.php")?>
        </div>

	</div>


    <?php include("part/footer.php")?>

    <script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="../public/js/slick.min.js" type="text/javascript"></script>
    <script src="../public/js/main.js?01" type="text/javascript"></script>

    <script>
        $('.menu-catalog').addClass('active');
    </script>

</body>

</html>
