<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<![endif]-->
	<title>Your Website</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?03" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>

    <!--    <link rel="stylesheet" href="public/css/main.min.css?04" type="text/css" />-->
</head>

<body>

	<header>
            <?php include("../page/part/menu.php")?>
	</header>
	
	<div class="service-page-blocks">

        <div class="service-block-top">
            <div class="container2">
                <div class="offset-left-menu">
                    <div class="breadcrumbs">
                        <a href="/">Главная</a>
                        <a href="catalog.php">Тарифы и услуги</a>
                        <span>Помощь в получении ЭЦП</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="service_first_block">
            <div class="container">
                <h1>Помощь в получении ЭЦП</h1>

                <h2>Описание услуги</h2>
                <p class="center title-descr">
                    Сэкономьте свое время и доверьте процесс получения ЭЦП нашимспециалистам!В рамках партнерского соглашения с удостоверяющим центром получить новую или перевыпустить неактивную ЭЦП получится еще быстрее.
                </p>
                <div class="row title-text">
                    <div class="col-sm-6">
                        <p class="title">Если у вас нет ЭЦП:</p>
                        <ul>
                            <li>Опишем четкую последовательность действий для получения ЭЦП</li>
                            <li>Подготовим необходимые документы</li>
                            <li>Свяжемся с удостоверяющим центром и оформим заявку</li>
                            <li>Закажем новую ЭЦП в ближайший к вам офис УЦ</li>
                            <li>Уведомим, когда подпись будет готова</li>
                        </ul>

                    </div>
                    <div class="col-sm-6">
                        <p class="title">Если у вашей ЭЦП истек срок действия:</p>
                        <ul>
                            <li>Расскажем, как снова активировать подпись</li>
                            <li>Подготовим необходимые документы</li>
                            <li>Свяжемся с удостоверяющим центром и оформим заявку</li>
                            <li>Уведомим, когда ваша подпись снова будет активна</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--div class="reviews row">
            <div class="col-sm-6 img-people">
                &nbsp;
            </div>
            <div class="col-sm-6 article dark">
                <div class="js-slider reviews-slider">
                    <div class="reviews_item">
                        <div class="quote quote_offset">
                            <p class="reviews_name"><span>Константинопольский</span> Константин Константинович</p>
                            <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                            <p class="reviews_text">
                                Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                                Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                            </p>
                        </div>
                        <div class="quote_offset">
                            <a class="btn btn-arrow">Все отзывы</a>
                        </div>
                    </div>
                    <div class="reviews_item ">
                        <div class="quote quote_offset">
                            <p class="reviews_name"><span>Second</span> Константин Константинович</p>
                            <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                            <p class="reviews_text">
                                Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                                Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                            </p>
                        </div>
                        <div class="quote_offset">
                            <a class="btn btn-arrow">Все отзывы</a>
                        </div>
                    </div>

                </div>
            </div>
        </div-->

        <div class="answers-bg">
            <div class="answers_title">
                <h2>Стоимость услуги</h2>
            </div>
        </div>

        <div class="products">
            <div class="container">
                <div class="row products_item_row">
                    <div class="products_item">
                        <p class="title">Ускоренное получение электронной цифровой подписи<br></p>
                        <p class="title2">ПОДГОТОВКА ДОКУМЕНТОВ</p>
                        <p class="descr">Готовим пакет документов для заявки</p>
                        <p class="title2">ЗАКАЗ ЭЛЕКТРОННОЙ ПОДПИСИ</p>
                        <p class="descr">Заказываем электронную подпись в УЦ</p>
                        <p class="title2">ВЫДАЧА ЭЛЕКТРОННОЙ ПОДПИСИ В УЦ</p>
                        <p class="descr">Вы забираете носитель с электронной подписью в ближайшем удостоверяющем центре</p>
                        <div class="products_item_bottom">
                            <p class="price mt"><span>1600 <span class="sm">руб.</span></span></p>
                            <a class="btn btn-2">Выбрать</a>
                        </div>

                    </div>
                    <div class="products_item">
                        <p class="title">Ускоренное получение ЭЦП с доставкой в офис</p>
                        <p class="title2">ПОДГОТОВКА ДОКУМЕНТОВ</p>
                        <p class="descr">Готовим пакет документов для заявки</p>
                        <p class="title2">ЗАКАЗ ЭЛЕКТРОННОЙ ПОДПИСИ</p>
                        <p class="descr">Заказываем электронную подпись в ЦУ</p>
                        <p class="title2">ДОСТАВЛЯЕМ ЭЦП К ВАМ</p>
                        <p class="descr">Доставляем носитель с электронной подписью к вам домой или в офис</p>
                        <div class="products_item_bottom">
                            <p class="price mt"> <span>2600 <span class="sm">руб.</span></span></p>
                            <a class="btn btn-2">Выбрать</a>
                        </div>

                    </div>
                    <div class="products_item">
                        <p class="title">Ускоренное получение ЭЦП с доставкой в офис и настройкой компьютера</p>
                        <p class="title2">ПОДГОТОВКА ДОКУМЕНТОВ И ЗАКАЗ ЭЦП</p>
                        <p class="descr">Готовим пакет документов и заказываем электронную подпись</p>
                        <p class="title2">ДОСТАВЛЯЕМ ЭЦП К ВАМ</p>
                        <p class="descr">Доставляем носитель с электронной подписью домой или в офис</p>
                        <p class="title2">НАСТРОЙКА КОМПЬТЕРА ДЛЯ РАБОТЫ С ЭЦП</p>
                        <p class="descr">Настраиваем ваш компьютер для работы с ЭЦП по удалённому доступу</p>
                        <div class="products_item_bottom">
                            <p class="price old mt"><span>3000 <span class="sm">руб.</span> </span></p>
                            <p class="price"> <span>2600 <span class="sm">руб.</span></span></p>
                            <a class="btn btn-2">Выбрать</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="answers answers-bg">

            <div class="answers_title">
                <h2>Часто задаваемые вопросы<br> по получению ЭЦП</h2>
            </div>

            <div class="row answers-block">

                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content">
                            <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                            <div class="description">
                                <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content">
                            <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                            <div class="description">
                                <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content">
                            <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                            <div class="description">
                                <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content">
                            <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                            <div class="description">
                                <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 ">
                    <div class="item">
                        <div class="item_content item_content_bottom">
                            <div class="title">Почему стремится к нулю сумма ряда?</div>
                            <div class="description">
                                <p> Достаточное условие сходимости охватывает нормальный натуральный логарифм,
                                    что неудивительно. Экстремум функции концентрирует интеграл по бесконечной области.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content  item_content_bottom">
                            <div class="title">Почему востребована огибающая семейства прямых?</div>
                            <div class="description">
                                <p>Сходящийся ряд обуславливает ротор векторного поля, таким образом сбылась мечта идиота - утверждение полностью доказано. Предел последовательности развивает косвенный контрпример. Аффинное преобразование, исключая очевидный случай, отображает интеграл от функции, обращающейся в бесконечность вдоль линии, что неудивительно./p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content  item_content_bottom">
                            <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                            <div class="description">
                                <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="item">
                        <div class="item_content  item_content_bottom">
                            <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                            <div class="description">
                                <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                    Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                    и самостоятельно заполнить часть документов.</p>
                                <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form_block">
            <?php include("part/contacts-form.php")?>
        </div>
	</div>

    <?php include("part/footer.php")?>

    <script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="../public/js/slick.min.js" type="text/javascript"></script>
    <script src="../public/js/main.js?01" type="text/javascript"></script>

    <script>
        $('.menu-catalog').addClass('active');
    </script>

</body>

</html>
