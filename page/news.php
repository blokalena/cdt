<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Электронные торги - это мы</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?01" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>


</head>

<body>

<header>
        <?php include("../page/part/menu.php")?>
</header>

<div class="info-page-blocks">

    <div class="block-top bg-gray">
        <div class="container2">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>информация</span>
            </div>
        </div>
    </div>

    <div class="container2 row">
        <div class="articles_menu_block">
            <?php include("../page/part/menu2_left.php")?>

            <div class="article_preview article_preview_single" style="background-image: url('/public/img/news/bg-left.png')">
                <div class="article_preview_descr">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"</p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>

        </div>

        <div class="articles_preview_block row">
            <div class="col-sm-4">
                <div class="article_preview_bg bg-1">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="article_preview_bg bg-2">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="article_preview_bg bg-3">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="article_preview_bg bg-2">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="article_preview_bg bg-3">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="article_preview_bg bg-1">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="article_preview_bg bg-1">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="article_preview_bg bg-2">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="article_preview_bg bg-3">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
            </div>


        </div>
    </div>

 </div>


<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-info').addClass('active');
    $('.menu2_news').addClass('active');
</script>


<script src="../public/js/Chart.min.js"></script>
<script src="../public/js/myChart.js"></script>

</body>

</html>
