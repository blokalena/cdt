<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Электронные торги - это мы</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?02" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>


</head>

<body>

<header>
    <?php include("../page/part/menu.php")?>
</header>

<div class="bargaining-page-blocks">

    <div class="block-top bg-gray">
        <div class="container2">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>Контакты</span>
            </div>
        </div>

    </div>


    <div class="contact_descr_block row">
        <div class="col-sm-6 item brt off-left">
            <div class="content">
                <div class="text">
                    <h2 class="underline">Отдел продаж</h2>
                    <p class="gray" >Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. </p>
                    <div class="contacts">
                        <p><a href="tel:+76669170711">Тел.: 8(666) 917-07-11</a> </p>
                        <p><a href="mailto:help@cdtrf.ru">help@cdtrf.ru</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 item blt off-right">
            <div class="content">
                <div class="text">
                    <h2 class="underline">Тех поддержка</h2>
                    <p class="gray">Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. </p>
                    <div class="contacts">
                        <p><a href="tel:+76669170711">Тел.: 8(666) 917-07-11</a> </p>
                        <p><a href="mailto:help@cdtrf.ru">help@cdtrf.ru</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hover-grey-blocks">
        <div class="container3">
            <h2>Руководители отделов</h2>
            <div class="row hover-grey-box">
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <div class="image"><img alt="people_1" src="../public/img/bargaining/people_1.png"></div>
                            <p class="name">Виктор Эмиль Марсден</p>
                            <p class="position">Журналист, переводчик, публицист</p>
                            <p><a href="" class="arrow">Написать письмо</a></p>
                        </div>
                        <div class="text">
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. 
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <div class="image"><img alt="people_2" src="../public/img/bargaining/people_2.png"></div>
                            <p class="name">Александр Степанович Апфельбаум</p>
                            <p class="position">Инженер-энергетик по техническому развитию</p>
                            <p><a href="" class="arrow">Написать письмо</a></p>
                        </div>
                        <div class="text">
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. 
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <div class="image"><img alt="people_3" src="../public/img/bargaining/people_3.png"></div>
                            <p class="name">Александра Ярославовна Гольденрудин</p>
                            <p class="position">Инженер-энергетик по техническому развитию</p>
                            <p><a href="" class="arrow">Написать письмо</a></p>
                        </div>
                        <div class="text">
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. 
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <div class="image"><img alt="people_1" src="../public/img/bargaining/people_1.png"></div>
                            <p class="name">Виктор Эмиль Марсден</p>
                            <p class="position">Журналист, переводчик, публицист</p>
                            <p><a href="" class="arrow">Написать письмо</a></p>
                        </div>
                        <div class="text">
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. 
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <div class="image"><img alt="people_2" src="../public/img/bargaining/people_2.png"></div>
                            <p class="name">Виктор Эмиль Марсден</p>
                            <p class="position">Журналист, переводчик, публицист</p>
                            <p><a href="" class="arrow">Написать письмо</a></p>
                        </div>
                        <div class="text">
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. 
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <div class="image"><img alt="people_3" src="../public/img/bargaining/people_3.png"></div>
                            <p class="name">Виктор Эмиль Марсден</p>
                            <p class="position">Журналист, переводчик, публицист</p>
                            <p><a href="" class="arrow">Написать письмо</a></p>
                        </div>
                        <div class="text">
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form_block form-left form-white">
        <?php include("part/contacts-form.php")?>
    </div>

</div>


<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-about').addClass('active');
</script>


<script src="../public/js/Chart.min.js"></script>
<script src="../public/js/myChart.js"></script>

</body>

</html>
