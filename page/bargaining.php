<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Торги по банкротству</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?02" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>


</head>

<body>

<header>
    <?php include("../page/part/menu.php")?>
</header>

<div class="bargaining-page-blocks">

    <div class="bargaining-block-top">
        <div class="container2">
            <div class="breadcrumbs breadcrumbs_white">
                <a href="/">Главная</a>
                <a href="catalog.php">Тарифы и услуги</a>
                <span>Торги по банкротству</span>
            </div>
        </div>
        <div class="container">
            <h1>Торги по банкротству</h1>
            <p class="title_txt">На нашей электронной торговой площадке мы помогаем арбитражным управляющим и организаторам торгов реализовывать имущество должников по 127-ФЗ.</p>
            <div class="row blocks">
                <div class="col-sm-4">
                    <div class="title">2 место</div>
                    <div class="descr">В РЕЙТИНГЕ ТОРГОВЫХ ПЛОЩАДОК «ЭТП ТОП-10»</div>
                </div>
                <div class="col-sm-4">
                    <div class="title">17000+</div>
                    <div class="descr">ТОРГОВ С 2010 ГОДА</div>
                </div>
                <div class="col-sm-4">
                    <div class="title">500 000+</div>
                    <div class="descr">МЛН РУБЛЕЙ ОБЪЁМ ПРОДАННЫХ ТОВАРОВ И УСЛУГ НА НАШЕЙ ЭТП</div>
                </div>
            </div>
        </div>
    </div>


    <div class="bargaining_descr_block">
        <h2>ПРЕИМУЩЕСТВА НАШЕЙ ЭТП</h2>
        <div class="row">
            <div class="item brt off-left">
                <div class="content">
                    <div class="head row">
                        <img src="/public/img/bargaining/ico-1.png">
                        <p>НАХОДИМ ПОКУПАТЕЛЕЙ</p>
                    </div>
                    <div class="text">
                        <p>У нас есть налаженные контакты с инвесторами и покупателями в разных сферах, которые закупают имущество на торгах. Также мы продвигаем торги в соцсетях, на досках объявлений, через контекстную рекламу. Помогаем покупателям с зарегистрироваться на торговой площадке и участвовать в торгах самим или через агента.</p>
                    </div>
                </div>
            </div>
            <div class="item blt off-right">
                <div class="content">
                    <div class="head row">
                        <img src="/public/img/bargaining/ico-2.png">
                        <p>ВСЕСТОРОННЕ ПОМОГАЕМ С ПРОВЕДЕНИЕМ ТОРГОВ</p>
                    </div>
                    <div class="text">
                        <p>Если у вас не хватает опыта или нет желания вести торги от и до, мы </p>
                        <ul>
                            <li>поможем консультацией</li>
                            <li>полностью возьмём на себя проведение одного или нескольких этапов торгов</li>
                            <li>проведём торги «под ключ» и реализуем имущество с наибольшей прибылью</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-2-blocks">
        <div  class="bargaining_descr_block">
            <div class="row">
                <div class="item brb off-left">
                    <div class="content">
                        <div class="head row">
                            <img src="/public/img/bargaining/ico-3.png">
                            <p>БЕРЁМ НА СЕБЯ ДОКУМЕНТООБОРОТ</p>
                        </div>
                        <div class="text">
                            <p>Возьмём на себя всё – от составления положения о торгах до обмена документами с ЕФРСБ, подачи публикаций в «Коммерсант» и другие СМИ, подготовки и публикации протоколов. Проследим за тем, чтобы сроки сроки подачи документов соответствовали законодательству.</p>

                        </div>
                    </div>
                </div>
                <div class="item blb off-right">
                    <div class="content">
                        <div class="head row">
                            <img src="/public/img/bargaining/ico-4.png">
                            <p>ГИБКОСТЬ</p>
                        </div>
                        <div class="text">
                            <p>Если у вас нестандартная ситуация или нужны особые условия – не оставим вас один на один с проблемой и найдём решение.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="graph-block">
            <div class="container">
                <h1>КОЛИЧЕСТВО ТОРГОВ НА НАШЕЙ ЭТП</h1>
                <div class="row ie-hide">
                    <div class="col-sm-4">
                        <div class="offset-right chart_bg_w">
                            <canvas id="chartArea" class="" width="100%" height="100"></canvas>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="offset-left chart_bg_w">
                            <canvas id="chartLine" class="" width="100%" height="50" ></canvas>
                        </div>
                    </div>
                </div>
                <div class="ie-show">
                    <img src="/public/img/graph.png">
                </div>
            </div>
        </div>
    </div>

    <div class="bargaining_text_block">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="offset-right">
                        <p>Выставление имущества на электронные торги – не только требование законодательства. Но и возможность продать его инвесторам или покупателям из другого региона или страны по максимальной стоимости для удовлетворения требований кредиторов.</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="offset-left">
                        <p>Участвовать в торгах на ЭТП просто, нужна только электронная подпись. Заказать её вы можете у нас. Мы принимаем заявки от юридических лиц, индивидуальных предпринимателей и физических лиц. .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="service_products_v2">
        <div class="container row">
            <div class="products_table">
                <div class="products_row ie-bg-gray">
                    <div class="products_cell title">
                        <p>Получение ЭЦП в стандартном порядке за 72 часа</p>
                    </div>
                    <div class="products_cell price">
                        <p>Бесплатно</p>
                    </div>
                    <div class="products_cell">
                        <a class="btn">Выбрать </a>
                    </div>
                </div>
                <div class="products_row main">
                    <div class="products_cell title">
                        <p>Ускоренное получение ЭЦП за 3 часа</p>
                    </div>
                    <div class="products_cell price">
                        <p class="old"><span>3000<span class="sm">руб.</span></span></p>
                        <p>2600<span class="sm">руб.</span></p>
                    </div>
                    <div class="products_cell">
                        <a class="btn">Выбрать </a>
                    </div>
                </div>
                <div class="products_row ie-bg-gray">
                    <div class="products_cell title">
                        <p>Ускоренное получение ЭЦП
                            с доставкой в офис</p>
                    </div>
                    <div class="products_cell price">
                        <p>3850<span class="sm">руб.</span></p>
                    </div>
                    <div class="products_cell">
                        <a class="btn">Выбрать </a>
                    </div>
                </div>
                <div class="products_row">
                    <div class="products_cell title">
                        <p>Ускоренное получение ЭЦП с доставкой в офис и настройкой компьютера</p>
                    </div>
                    <div class="products_cell price">
                        <p>4500<span class="sm">руб.</span></p>
                    </div>
                    <div class="products_cell">
                        <a class="btn">Выбрать </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--div class="reviews row">
        <div class="col-sm-6 img-people">
            &nbsp;
        </div>
        <div class="col-sm-6  article dark">
            <div class="js-slider reviews-slider">
                <div class="reviews_item">
                    <div class="quote quote_offset">
                        <p class="reviews_name"><span>Константинопольский</span> Константин Константинович</p>
                        <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                        <p class="reviews_text">
                            Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                            Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                        </p>
                    </div>
                    <div class="quote_offset">
                        <a class="btn">Все отзывы</a>
                    </div>
                </div>
                <div class="reviews_item ">
                    <div class="quote quote_offset">
                        <p class="reviews_name"><span>Second</span> Константин Константинович</p>
                        <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                        <p class="reviews_text">
                            Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                            Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                        </p>
                    </div>
                    <div class="quote_offset">
                        <a class="btn">Все отзывы</a>
                    </div>
                </div>

            </div>
        </div>
    </div-->

    <div class="hover-grey-blocks">
        <div class="container3">
            <div class="row hover-grey-box not-fix fix-sm">
                <div class="col-sm-4">
                    <div class="content ">
                        <div class="head row">
                            <p class="name">Покупателю</p>
                        </div>
                        <div class="text">
                            Поможем принять участие в закупке товаров или услуг для государственных и негосударственных организаций
                        </div>
                        <p><a href="" class="arrow">Перейти</a></p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <p class="name">Организатору торгов</p>
                        </div>
                        <div class="text">
                            Поможем найти лучшего поставщика, исходя из ваших требований по цене и качеству
                        </div>
                        <p><a href="" class="arrow">Перейти</a></p>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <p class="name">Закупки по 223 ФЗ</p>
                        </div>
                        <div class="text">
                            Поможем с подготовкой ТЗ и документации, проследим за соблюдением законодательства, поможем отбиться от жалоб и претензий.
                        </div>
                        <p><a href="" class="arrow">Перейти</a></p>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <!--    <div class="colors-box-block">-->
    <!--        <div class="">-->
    <!--            <div class="row">-->
    <!--                    <div class="col-sm-3 item">-->
    <!--                        <p class="title">Покупателю</p>-->
    <!--                        <p class="descr">Поможем принять участие в закупке товаров или услуг для государственных и негосударственных организаций</p>-->
    <!--                    </div>-->
    <!--                    <div class="col-sm-3 item">-->
    <!--                        <p class="title">Организатору торгов</p>-->
    <!--                        <p class="descr">Поможем найти лучшего поставщика, исходя из ваших требований по цене и качеству</p>-->
    <!--                    </div>-->
    <!--                    <div class="col-sm-3 item">-->
    <!--                        <p class="title">Закупки по 223 ФЗ</p>-->
    <!--                        <p class="descr">Поможем с подготовкой ТЗ и документации, проследим за соблюдением законодательства, поможем отбиться от жалоб и претензий.</p>-->
    <!--                    </div>-->
    <!--                    <div class="col-sm-3 item">-->
    <!--                        <p class="title">Все услуги</p>-->
    <!--                        <p class="descr">Мы оказываем множество услуг для поставщиков и организаторов закупок. Выберите услугу, которая интересует вас.</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="form_block">
        <?php include("part/contacts-form.php")?>
    </div>
</div>


<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-catalog').addClass('active');
</script>

<script src="../public/js/Chart.min.js"></script>
<script src="../public/js/myChart.js"></script>

</body>

</html>
