<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Список СРО</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?02" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>


</head>

<body>

<header>
        <?php include("../page/part/menu.php")?>
</header>

<div class="sro-page-blocks">

    <div class="sro_block_top">
        <div class="container2">
            <div class="breadcrumbs breadcrumbs_white">
                <a href="/">Главная</a>
                <span>Список СРО</span>
            </div>
        </div>
        <div class="container">
            <h1>Список СРО</h1>
            <p class="title_txt center">
                АО Центр дистанционных торгов является членом СРО Союз Торговых Электронных Площадок (СТЭП).
                Мы успешно аккредитованы более чем на 30 саморегулируемых организациях арбитражных управляющихв качестве оператора электронной площадки по продаже имущества должников.
            </p>
        </div>
    </div>


    <div class="sro_descr_block">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 ">
                    <div class="item">
                        <p class="title">Союз "СРО АУ СЗ"</p>
                        <p>Союз "Саморегулируемая организация арбитражных управляющих Северо-Запада"</p>
                    </div>
                    <div class="item">
                        <p class="title">СРО ААУ "Евросиб"</p>
                        <p>Ассоциация Евросибирская саморегулируемая организация арбитражных управляющих</p>
                    </div>
                    <div class="item">
                        <p class="title">Ассоциация "Первая СРО АУ"</p>
                        <p>Ассоциация "Первая Саморегулируемая Организация Арбитражных Управляющих, зарегистрированная в едином государственном реестре саморегулируемых организаций арбитражных управляющих"</p>
                    </div>
                    <div class="item">
                        <p class="title">МСО ПАУ</p>
                        <p>Ассоциация "Межрегиональная саморегулируемая организация профессиональных арбитражных управляющих"</p>
                    </div>
                    <div class="item">
                        <p class="title">НП "ЦФОП АПК"</p>
                        <p>Некоммерческое партнерство "Центр финансового оздоровления предприятий агропромышленного комплекса"</p>
                    </div>
                </div>
                <div class="col-sm-6 ">
                    <div class="item">
                        <p class="title">НП "ЦФОП АПК"</p>
                        <p>Некоммерческое партнерство "Центр финансового оздоровления предприятий агропромышленного комплекса"</p>
                    </div>
                    <div class="item">
                        <p class="title">СОЮЗ "АУ "ПРАВОСОЗНАНИЕ"</p>
                        <p>СОЮЗ "АРБИТРАЖНЫХ УПРАВЛЯЮЩИХ "ПРАВОСОЗНАНИЕ"</p>
                    </div>
                    <div class="item">
                        <p class="title">Союз "СРО АУ СЗ"</p>
                        <p>Союз "Саморегулируемая организация арбитражных управляющих Северо-Запада"</p>
                    </div>
                    <div class="item">
                        <p class="title">Союз АУ "СРО "Северная Столица"</p>
                        <p>Союз арбитражных управляющих "Саморегулируемая организация "Северная Столица"</p>
                    </div>
                    <div class="item">
                        <p class="title">СРО ААУ "Евросиб"</p>
                        <p>Ассоциация Евросибирская саморегулируемая организация арбитражных управляющих</p>
                    </div>
                </div>
            </div>
            <div class="row center">
                <a class="btn btn-oval">Показать все</a>
            </div>

        </div>
    </div>


    <div class="form_block">
        <h2>Возникли вопросы?</h2>
        <?php include("part/contacts-form.php")?>
    </div>
</div>


<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-info').addClass('active');
</script>


</body>

</html>
