<div id="top-menu" class="top-main-menu relative" >
    <div class="container2">
        <div  class="navbar-menu row" >

            <div class="logotype navbar-left">
                <a href="/" class="soft"><img src="/public/img/logo.png"></a>
            </div>

            <nav class="navbar">
                <!-- Brand and toggle get grouped for better mobile display -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span></span>
                </button>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul id="nav" class="nav navbar-nav navbar-left">
                        <li><a href="/" class="menu-main">Главная</a></li>
                        <li>
                            <a href="/page/catalog.php" class="menu-catalog">Тарифы и услуги</a><i class="nav-arrow"></i>
                            <ul>
                                <li><a href="/page/service_card.php">Карточка услуги</a></li>
                                <li><a href="/page/service_card_2.php">Карточка услуги 2</a></li>
                                <li><a href="/page/bargaining.php">Торги по банкротству</a></li>
                                <li><a href="/page/procurement.php">Закупки по 223 ФЗ</a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="#" class="menu-electro">Электронная подпись</a><i class="nav-arrow"></i>
                            <ul>
                                <li><a href="/page/service_card_2.php">Получить подпись</a></li>
                                <li><a href="/page/list-service.php">Список УЦ</a> </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/page/news_and_article.php" class="menu-info">Информация</a><i class="nav-arrow"></i>
                            <ul>
                                <li><a href="/page/about.php">О компании</a></li>
                                <li><a href="/page/news_and_article.php">Статьи</a></li>
                                <li><a href="/page/news.php">Новости</a></li>
                                <li><a href="/page/sro.php">СРО</a></li>
                                <li><a href="/page/contacts.php">Контакты</a></li>
                            </ul>
                        </li>
                    </ul>

                    <div class="nav_right_block  navbar-left nav-arrow trading-platforms">
                        <ul class="nav navbar-nav">
                            <li class="trading-platforms">
                                <a>Торговые площадки</a><i class="nav-arrow"></i>
                                <ul>
                                    <li><a href="/">Банкротные торги</a></li>
                                    <li><a href="/">Закупки</a></li>
                                    <li><a href="/">Арестованное имущество</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div><!-- /.navbar-collapse -->

            </nav>

        </div>
    </div>
</div>