<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content container">
            <div class="modal-header ">
                <button type="button" class="close modalclose js-modalClose" data-dismiss="modal" aria-label="Close"></button>
                <h2 class="modal-title underline">Подпишитесь на нашу информационную рассылку</h2>
            </div>
            <div class="modal-body">
                <div class=" form_block form-white">
                    <form class="row">
                        <div class="col-sm-6 control"><div class="offset_right"><input type="text" placeholder="Имя*"></div></div>
                        <div class="col-sm-6 control"><div class="offset_left"><input type="text" placeholder="Телефон*"></div></div>
                        <div class="col-sm-8 control left">
                            <label class="row">
                                <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                                <p class="checkbox_txt">Я согласен с  условиями обработки персональных данных</p>
                            </label>
                        </div>
                        <div class="col-sm-4 control right">
                            <button class="btn">Подписаться</button>
                        </div>
                    </form>
                </div>

                <div class="results"></div>
            </div>

        </div>
    </div>
</div>