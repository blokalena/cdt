<div class="container">
        <div class="col-sm-8">
            <h2 class="underline">Пишите нам</h2>
            <form class="row">
                <p>Наш менеджер свяжется с вами в течение рабочего дня и обязательно поможет</p>
                <div class="col-sm-12 control"><input type="text" placeholder="Имя*"></div>
                <div class="col-sm-6 control"><div class="offset"><input type="email" placeholder="E-mail*"></div></div>
                <div class="col-sm-6 control"><div class="offset"><input type="text" placeholder="Телефон*"></div></div>
                <div class="col-sm-12 control"><textarea placeholder="Ваш вопрос"></textarea></div>
                <div class="col-sm-8 control left">
                    <label class="row">
                        <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                        <p class="checkbox_txt">Я согласен с  условиями обработки персональных данных</p>
                    </label>
                </div>
                <div class="col-sm-4 control right">
                    <button class="btn">ОТПРАВИТЬ</button>
                </div>
            </form>
        </div>
        <div class="col-sm-4">
            <h2 class="underline">Cвязаться</h2>
            <p>АО «Центр дистанционных торгов»</p>
            <div class="contacts_ico_block">
                <div>
                    <i class="ico ico-map"></i>
                    <p>191028 г. Санкт-Петербург, ул. Чайковского, д.12</p>
                </div>
                <div>
                    <i class="ico ico-mail"></i>
                    <p><a href="mailto:help@cdtrf.ru">help@cdtrf.ru</a></p>
                </div>
                <div>
                    <i class="ico ico-phone"></i>
                    <p><a href="tel:78126464818">+7 (812) 64-64-818</a> <br>
                        <a href="tel:78126464818">+7 (812) 64-64-810</a></p>
                </div>
                <div>
                    <i class="ico ico-site"></i>
                    <p><a href="www.cdtrf.ru">www.cdtrf.ru</a> </p>
                </div>
            </div>
        </div>
    </div>
