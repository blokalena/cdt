<div class="page_footer">
    <div class="container">
        <div class="row footer_top_block">
            <div>
                <div class="logo">
                    <img src="/public/img/ico/logo_foter.png">
                </div>
            </div>
            <div class="footer_address">
                <p>АО «Центр дистанционных торгов» 191187, Санкт-Петербург,<br> ул. Чайковского, д.12 +7 (812) 64 64 818</p>
            </div>
            <div class="footer_email">
                <p class="js-modalOpen">help@cdtrf.ru</p>
            </div>
            <div class="social-color">
                <i class="ico social vk"></i>
                <i class="ico social fb"></i>
                <i class="ico social ub"></i>
                <i class="ico social inst"></i>
            </div>
        </div>
        <div class="row footer_bottom_block">
            <p>© Сайт создан маркетинговым агентством <a>Жуков и Архангельский</a></p>
        </div>
    </div>
</div>

<?php include("modal.php")?>