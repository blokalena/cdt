<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<![endif]-->
	<title>Your Website</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?03" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>

    <!--    <link rel="stylesheet" href="public/css/main.min.css?04" type="text/css" />-->
</head>

<body>

	<header>
            <?php include("../page/part/menu.php")?>
	</header>
	
	<div class="service-page-blocks">

        <div class="service-block-top">
            <div class="container2">
                <div class="breadcrumbs">
                    <a href="/">Главная</a>
                    <a href="catalog.php">Тарифы и услуги</a>
                    <span>Помощь в получении ЭЦП</span>
                </div>
            </div>
        </div>

        <div class="service_first_block">
            <div class="container">
                <h1>Помощь в получении ЭЦП</h1>

                <h2>Описание услуги</h2>
                <p class="center title-descr">
                    Сэкономьте свое время и доверьте процесс получения ЭЦП нашимспециалистам!В рамках партнерского соглашения с удостоверяющим центром получить новую или перевыпустить неактивную ЭЦП получится еще быстрее.
                </p>
                <div class="row title-text">
                    <div class="col-sm-6">
                        <p class="title">Если у вас нет ЭЦП:</p>
                        <ul>
                            <li>Опишем четкую последовательность действий для получения ЭЦП</li>
                            <li>Подготовим необходимые документы</li>
                            <li>Свяжемся с удостоверяющим центром и оформим заявку</li>
                            <li>Закажем новую ЭЦП в ближайший к вам офис УЦ</li>
                            <li>Уведомим, когда подпись будет готова</li>
                        </ul>

                    </div>
                    <div class="col-sm-6">
                        <p class="title">Если у вашей ЭЦП истек срок действия:</p>
                        <ul>
                            <li>Расскажем, как снова активировать подпись</li>
                            <li>Подготовим необходимые документы</li>
                            <li>Свяжемся с удостоверяющим центром и оформим заявку</li>
                            <li>Уведомим, когда ваша подпись снова будет активна</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="answers-bg">
            <div class="answers_title">
                <h2>Стоимость услуги</h2>
            </div>
        </div>

        <div class="service_products_v2">
            <div class="container row">
                <div class="products_table">
                    <div class="products_row ie-bg-gray">
                        <div class="products_cell title">
                            <p>Получение ЭЦП в стандартном порядке за 72 часа</p>
                        </div>
                        <div class="products_cell price">
                            <p>Бесплатно</p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                    <div class="products_row main">
                        <div class="products_cell title">
                            <p>Ускоренное получение ЭЦП за 3 часа</p>
                        </div>
                        <div class="products_cell price">
                            <p class="old"><span>3000<span class="sm">руб.</span></span></p>
                            <p>2600<span class="sm">руб.</span></p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                    <div class="products_row ie-bg-gray">
                        <div class="products_cell title">
                            <p>Ускоренное получение ЭЦП
                                с доставкой в офис</p>
                        </div>
                        <div class="products_cell price">
                            <p>3850<span class="sm">руб.</span></p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                    <div class="products_row ">
                        <div class="products_cell title">
                            <p>Ускоренное получение ЭЦП с доставкой в офис и настройкой компьютера</p>
                        </div>
                        <div class="products_cell price">
                            <p>4500<span class="sm">руб.</span></p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="answers answers-bg">

            <div class="answers_title">
                <h2>Часто задаваемые вопросы<br> по получению ЭЦП</h2>
            </div>

            <div class="row answers-block">

                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content">
                                <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                                <div class="description">
                                    <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content">
                                <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                                <div class="description">
                                    <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content">
                                <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                                <div class="description">
                                    <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content">
                                <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                                <div class="description">
                                    <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 ">
                        <div class="item">
                            <div class="item_content item_content_bottom">
                                <div class="title">Почему стремится к нулю сумма ряда?</div>
                                <div class="description">
                                    <p> Достаточное условие сходимости охватывает нормальный натуральный логарифм,
                                        что неудивительно. Экстремум функции концентрирует интеграл по бесконечной области.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content  item_content_bottom">
                                <div class="title">Почему востребована огибающая семейства прямых?</div>
                                <div class="description">
                                    <p>Сходящийся ряд обуславливает ротор векторного поля, таким образом сбылась мечта идиота - утверждение полностью доказано. Предел последовательности развивает косвенный контрпример. Аффинное преобразование, исключая очевидный случай, отображает интеграл от функции, обращающейся в бесконечность вдоль линии, что неудивительно./p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content  item_content_bottom">
                                <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                                <div class="description">
                                    <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="item">
                            <div class="item_content  item_content_bottom">
                                <div class="title">Как долго будет изготавливаться ЭЦП, если заказать ее сегодня?</div>
                                <div class="description">
                                    <p>Пакет документов зависит от того, заказываете вы новую подпись или перевыпускаете старую. В любом случае наши специалисты сообщат вам, что именно нужно собрать и самостоятельно подадут заявку в удостоверяющий центр.
                                        Также для ускорения процесса выпуска или перевыпуска вы можете скачать
                                        и самостоятельно заполнить часть документов.</p>
                                    <a class="document pdf" href="#">Для получения новой подписи, 512 кб</a>
                                    <a class="document doc" href="#" >Для перевыпуска подписи, 408 кб</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>

        <div class="form_block">
            <?php include("part/contacts-form.php")?>
        </div>
	</div>


    <?php include("part/footer.php")?>

    <script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="../public/js/slick.min.js" type="text/javascript"></script>
    <script src="../public/js/main.js?01" type="text/javascript"></script>

    <script>
        $('.menu-catalog').addClass('active');
    </script>

</body>

</html>
