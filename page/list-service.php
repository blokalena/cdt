<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Your Website</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?01" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>

    <!--    <link rel="stylesheet" href="public/css/main.min.css?04" type="text/css" />-->
</head>

<body>

<header>
    <?php include("../page/part/menu.php")?>
</header>

<div class="list-service-page-blocks">

    <div class="list-service-page-block-top">
        <div class="container2">
            <div class="offset-left-menu">
                <div class="breadcrumbs breadcrumbs_white">
                    <a href="/">Главная</a>
                    <span>Список УЦ</span>
                </div>
            </div>
        </div>
        <div class="container">
            <h1 class="list-service_h1">Список удостоверяющих центров</h1>
            <p class="center title-descr">
                Список аккредитованных удостоверяющих центров, где можно получить ЭЦП. Перечень УЦ для оформления электронной цифровой подписи ЭЦП в Москве и других городах России и их официальных сайтов. Федеральные удостоверяющие центры ЭЦП имеют филиалы в РФ.                </p>
        </div>
    </div>

    <div class="container container-sm-100">

        <div class="article_preview_bg bg-color-list article dark">
            <div class="show">
                <p class="title">
                    Удостоверяющий центр «Тензор»                    </p>
            </div>
            <div class="text">
                <p>
                    Получить электронную подпись (ЭП) в удостоверяющем центре Тензор</p>
                <a href="#" class="btn btn-arrow">Перейти</a>
            </div>
        </div>
        <div class="row">
           <div class="col-sm-6">
               <div class="article_preview_bg bg-3">
                   <div class="show">
                       <p class="title">
                          Удостоверяющий центр «Такснет»                   </p>
                   </div>
                   <div class="text">
                       <p>
                           Получить электронную подпись (ЭП) в удостоверяющем центре Такснет </p>
                       <a href="#" class="btn btn-arrow">Перейти</a>
                   </div>
               </div>
           </div>
           <div class="col-sm-6">
               <div class="article_preview_bg bg-2">
                   <div class="show">
                       <p class="title">
                           Удостоверяющий центр «Сервер-Центр»                   </p>
                   </div>
                   <div class="text">
                       <p>
                           Получить электронную подпись (ЭП) в удостоверяющем центре Сервер-Центр </p>
                       <a href="#" class="btn btn-arrow">Перейти</a>
                   </div>
               </div>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="underline_list">
            <div class="underline_list_item">
                <p class="title">Удостоверяющий центр «IT&COM»</p>
                <a href="#">Получить электронную подпись (ЭП) в удостоверяющем центре IT&COM</a>
            </div>
            <div class="underline_list_item">
                <p class="title">Союз АУ "СРО "Северная Столица"</p>
                <a href="#">Союз арбитражных управляющих "Саморегулируемая организация "Северная Столица"</a>
            </div>
            <div class="underline_list_item">
                <p class="title">СРО ААУ "Евросиб"</p>
                <a href="#">Ассоциация Евросибирская саморегулируемая организация арбитражных управляющих</a>
            </div>
            <div class="underline_list_item">
                <p class="title">Ассоциация "Первая СРО АУ"</p>
                <a href="#">Ассоциация "Первая Саморегулируемая Организация Арбитражных Управляющих, зарегистрированная в едином государственном реестре саморегулируемых организаций арбитражных управляющих"</a>
            </div>
            <div class="underline_list_item">
                <p class="title">МСО ПАУ</p>
                <a href="#">Ассоциация "Межрегиональная саморегулируемая организация профессиональных арбитражных управляющих"</a>
            </div>
            <div class="center">
                <a class="btn btn-oval" href="#">Показать все </a>
            </div>
        </div>

    </div>


    <!--div class="form_block">
        <div class="container">
            <form>
                <p class="title">Возникли вопросы?</p>
                <p>Наш менеджер свяжется с вами в течение рабочего дня и обязательно поможет</p>
                <div class="col-sm-12 control"><input type="text" placeholder="Имя*"></div>
                <div class="col-sm-6 control"><div class="offset"><input type="email" placeholder="E-mail*"></div></div>
                <div class="col-sm-6 control"><div class="offset"><input type="text" placeholder="Телефон*"></div></div>
                <div class="col-sm-12 control"><textarea placeholder="Ваш вопрос"></textarea></div>
                <div class="col-sm-8 control left">
                    <label class="row">
                        <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                        <p class="checkbox_txt">Я согласен с  условиями обработки персональных данных</p>
                    </label>
                </div>
                <div class="col-sm-4 control right">
                    <button class="btn">Перезвоните мне</button>
                </div>
            </form>
        </div>
    </div-->
    <div class="form_block form-left">
        <?php include("part/contacts-form.php")?>
    </div>
</div>

<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-about').addClass('active');
</script>

</body>

</html>
