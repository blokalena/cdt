<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<![endif]-->
	<title>Your Website</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?01" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>

    <!--    <link rel="stylesheet" href="public/css/main.min.css?04" type="text/css" />-->
</head>

<body>

	<header>
            <?php include("../page/part/menu.php")?>
	</header>
	
	<div class="role-page-blocks">

        <div class="role_first_block">
            <div class="container">
                <h1>Уважаемый Сергей Александрович!</h1>
                <p class="center title-descr">
                    Мы помним, что вы проводили торги на площадке ЦДТ. Хотите продолжить сотрудничество? Предлагаем вам скидку 20% на получение или продление цифровой подписи (ЭЦП).</p>
            </div>
        </div>

        <div class="role_descr  bg-gray">
            <div class="container row">
                <div class="content">
                    <p>
                        Анализ и подготовка всех необходимых материалов для проведения торгов, в том числе консультация при составлении Положения о продаже имущества. Компоновка обязательных сообщений в СМИ и контроль сроков их публикации. Прием звонков и первичное консультирование потенциальных покупателей. Заключение договоров о задатке с потенциальными покупателями.
                    </p>
                </div>
            </div>
        </div>

        <div class="service_products_v2">
            <div class="container row">
                <div class="products_table">
                    <div class="products_row">
                        <div class="products_cell title">
                            <p>Получение ЭЦП в стандартном порядке за 72 часа</p>
                        </div>
                        <div class="products_cell price">
                            <p>Бесплатно</p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                    <div class="products_row main">
                        <div class="products_cell title">
                            <p>Ускоренное получение ЭЦП за 3 часа</p>
                        </div>
                        <div class="products_cell price">
                            <p class="old"><span>3000<span class="sm">руб.</span></span></p>
                            <p>2600<span class="sm">руб.</span></p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                    <div class="products_row">
                        <div class="products_cell title">
                            <p>Ускоренное получение ЭЦП
                                с доставкой в офис</p>
                        </div>
                        <div class="products_cell price">
                            <p>3850<span class="sm">руб.</span></p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                    <div class="products_row">
                        <div class="products_cell title">
                            <p>Ускоренное получение ЭЦП с доставкой в офис и настройкой компьютера</p>
                        </div>
                        <div class="products_cell price">
                            <p>4500<span class="sm">руб.</span></p>
                        </div>
                        <div class="products_cell">
                            <a class="btn">Выбрать </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--div class="reviews row">
            <div class="col-sm-6 img-people">
                &nbsp;
            </div>
            <div class="col-sm-6  article dark">
                <div class="js-slider reviews-slider">
                    <div class="reviews_item">
                        <div class="quote quote_offset">
                            <p class="reviews_name"><span>Константинопольский</span> Константин Константинович</p>
                            <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                            <p class="reviews_text">
                                Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                                Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                            </p>
                        </div>
                        <div class="quote_offset">
                            <a class="btn btn-arrow">Все отзывы</a>
                        </div>
                    </div>
                    <div class="reviews_item ">
                        <div class="quote quote_offset">
                            <p class="reviews_name"><span>Second</span> Константин Константинович</p>
                            <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                            <p class="reviews_text">
                                Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                                Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                            </p>
                        </div>
                        <div class="quote_offset">
                            <a class="btn btn-arrow">Все отзывы</a>
                        </div>
                    </div>

                </div>
            </div>
        </div-->

	</div>

    <?php include("part/footer.php")?>

    <script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
    <script src="../public/js/slick.min.js" type="text/javascript"></script>
    <script src="../public/js/main.js?01" type="text/javascript"></script>

    <script>
        $('.menu-catalog').addClass('active');
    </script>

</body>

</html>
