<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Электронные торги - это мы</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?01" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>


</head>

<body>

<header>
        <?php include("../page/part/menu.php")?>
</header>

<div class="info-page-blocks">

    <div class="block-top bg-gray">
        <div class="container2">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>информация</span>
            </div>
        </div>
    </div>

    <div class="container2 row">
        <div class="articles_menu_block">
            <?php include("../page/part/menu2_left.php")?>
        </div>

        <div class="articles_preview_block">
            <div class="articles_content_block">
                <div>
                    <p class="content_data">29 январь 2018</p>
                    <h2 class="underline">ПРЕКРАТИТЕ ПЕРЕПЛАЧИВАТЬ ЗА ПУБЛИКАЦИИ В КОММЕРСАНТЕ!</h2>
                    <div class="txt">
                        <p> Вряд-ли вы любите размещать сообщения в ЕФРСБ, Коммерсанте и других СМИ.   </p>
                        <p> Во-первых, это дорого. Например, Коммерсантъ берёт 216 рублей за см² или 8,5 рублей за 1 символ. Сообщение о больших торгах может стоить до 60000 рублей. И кредиторы точно будут не в восторге от таких затрат.  </p>
                        <p> Во-вторых, из-за платной подачи, приходится работать над сокращением текста. Что отнимает силы и время от другой работы. </p>
                        <p> В-третьих, нужно соблюсти все требования закона по содержанию и срокам подачи. А это лишние нервы и постоянный вопрос внутри «а всё ли у меня правильно»? </p>
                        <p> Что же делать, чтобы и закон соблюсти, и время сэкономить, и не раздражать кредиторов лишними расходами? </p>
                        <p> <strong>Как мы помогаем соблюдать закон и экономить 33% бюджета на публикации</strong>  </p>
                        <p> Прежде всего – оптимизируем и сокращаем текст сообщения. Наша практика говорит, что любой текст можно урезать на треть без потери смысла. Так зачем же переплачивать за лишние знаки, если текст за 60000 без потери качества превращается в текст за 40000?  </p>
                        <p> Затем проверяем текст на соответствие закону, подсказываем по срокам размещения. Дальше можем отдать готовое сообщение, чтобы вы сами его опубликовали. Или взять публикацию на себя – зависит от формата сотрудничества с нашей электронной торговой площадкой.   </p>
                        <p> <strong>Вы получаете</strong>  </p>
                        <p> - свободу от ненавистной рутинной процедуры;  </p>
                        <p> - 100% защиту от ошибок при подаче;   </p>
                        <p> - готовый текст в срок, даже если «надо прямо сейчас»;  </p>
                        <p> - экономию на штрафах за несвоевременную подачу;   </p>
                        <p> - здоровые нервы.   </p>
                    </div>
                    <p class="right">
                        <a class="btn btn-oval">назад</a>
                    </p>
                </div>
            </div>

            <div class="articles_right_block">
                <div class="article_preview_bg bg-2">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
                <div class="article_preview_bg bg-3">
                    <div class="show">
                        <p class="data">29 январь 2018</p>
                        <p class="title">
                            Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"
                        </p>
                    </div>
                    <div class="text">
                        <p>
                            Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю. Съешь еще этих мягких французских булок, да выпей чаю...
                        </p>
                    </div>
                </div>
                <div class="article_preview" style="background-image: url('/public/img/news/bg-left.png')">
                    <div class="article_preview_descr article dark full">
                        <div class="full_text ">
                            <p class="news-block_type">Событие</p>
                            <p class="data">29 январь 2018</p>
                            <p>Сняты обеспечительные меры в отношении торгов №0134117 по продаже имущества должника ЗАО "РСП ЦР"</p>

                            <p>
                                <a class="btn">принять участие </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

 </div>


<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-info').addClass('active');
    $('.menu2_bank').addClass('active');
</script>


<script src="../public/js/Chart.min.js"></script>
<script src="../public/js/myChart.js"></script>

</body>

</html>
