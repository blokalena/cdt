<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/html">
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if IE 8 ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <![endif]-->
    <title>Торги по банкротству</title>
    <link rel="stylesheet" href="../public/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="../public/css/index.css?01" type="text/css" />

    <script src="../public/js/ie/jquery.placeholder.min.js"></script>
    <script src="../public/js/ie/html5shiv.js"></script>
    <script src="../public/js/ie/respond.min.js"></script>


</head>

<body>

<header>
        <?php include("../page/part/menu.php")?>
</header>

<div class="procurement">

    <div class="bargaining-block-top">
        <div class="container2">
            <div class="breadcrumbs breadcrumbs_white">
                <a href="/">Главная</a>
                <a href="catalog.php">Тарифы и услуги</a>
                <span>Закупки по 223 ФЗ</span>
            </div>
        </div>
        <div class="container">
            <h1>Закупки по 223 ФЗ</h1>
            <p class="title_txt">На нашей электронной торговой площадке мы помогаем государственным и частным предприятиям проводить закупки в соответствии с 223-ФЗ.</p>
            <div class="row blocks">
                <div class="col-sm-4">
                    <div class="title">17%</div>
                    <div class="descr">ЗАКУПОЧНОГО БЮДЖЕТА, В СРЕДНЕМ, ЭКОНОМЯТ ПРЕДПРИЯТИЯ ПРИ ЗАКУПКЕ НА ЭЛЕКТРОННЫХ ТОРГАХ</div>
                </div>
                <div class="col-sm-4">
                    <div class="title">100%</div>
                    <div class="descr">СООТВЕТСТВИЕ ТРЕБОВАНИЯМ УФАС И ЗАКОНОДАТЕЛЬСТВУ</div>
                </div>
                <div class="col-sm-4">
                    <div class="title">250+</div>
                    <div class="descr">ТОРГОВ ПО 223-ФЗ ЕЖЕГОДНО</div>
                </div>
            </div>
        </div>
    </div>


    <div class="bargaining_descr_block">
        <h2>ПРЕИМУЩЕСТВА НАШЕЙ ЭТП</h2>
        <div class="row">
            <div class="col-sm-6 item brt off-left">
                <div class="content">
                    <div class="head row">
                        <img src="/public/img/bargaining/ico-1.png">
                        <p>ПОМОГАЕМ С ВЫБОРОМ ПОСТАВЩИКА</p>
                    </div>
                    <div class="text">
                        <p>Мы постоянно привлекаем на нашу электронную торговую площадку поставщиков разных видов товаров и услуг. Среди них точно найдётся тот, кто даст вам то, что ва нужно по цене и качеству.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item blt off-right">
                <div class="content">
                    <div class="head row">
                        <img src="/public/img/bargaining/ico-2.png">
                        <p>ВСЕСТОРОННЕ ПОМОГАЕМ С ПРОВЕДЕНИЕМ ТОРГОВ</p>
                    </div>
                    <div class="text">
                        <p>Если у вас не хватает опыта или нет желания вести торги от и до, мы поможем консультацией, полностью возьмём на себя проведение одного или нескольких этапов закупки, проведём закупку «под ключ»</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-2-blocks">
        <div  class="bargaining_descr_block">
            <div class="row">
                <div class="col-sm-6 item brb off-left">
                    <div class="content">
                        <div class="head row">
                            <img src="/public/img/bargaining/ico-3.png">
                            <p>БЕРЁМ НА СЕБЯ ДОКУМЕНТООБОРОТ</p>
                        </div>
                        <div class="text">
                            <p>Возьмём на себя оформление части или всей документации по закупкам – от составления положения и плана закупок, до подготовки и публикации протоколов и подачи отчётов в ЕИС.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 item blb off-right">
                    <div class="content">
                        <div class="head row">
                            <img src="/public/img/bargaining/ico-4.png">
                            <p>ЗАЩИЩАЕМ ОТ ЖАЛОБ И ИСКОВ</p>
                        </div>
                        <div class="text">
                            <p>Проведём закупку в полном соответствии с требованиями законодательства для исключения жалоб и исков. Если они последуют – защитим ваши интересы в территориальных ФАС.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="graph-block">
            <div class="container">
                <h1>КОЛИЧЕСТВО ТОРГОВ НА НАШЕЙ ЭТП</h1>
                <div class="graph-block">
                    <div class="container">
                        <h1>КОЛИЧЕСТВО ТОРГОВ НА НАШЕЙ ЭТП</h1>
                        <div class="row ie-hide">
                            <div class="col-sm-4">
                                <div class="offset-right chart_bg_w">
                                    <canvas id="chartArea" class="" width="100%" height="100"></canvas>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="offset-left chart_bg_w">
                                    <canvas id="chartLine" class="" width="100%" height="50" ></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="ie-show">
                            <img src="/public/img/graph.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bargaining_text_block">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="offset-right">
                        <p>Организация электронных закупок для предприятия – возможность сэкономить, или получить лучшую услугу на рынке. Поставщик же находит долговременного и платежеспособного клиента.</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="offset-left">
                        <p>Для организации или участия в торгах по 223-ФЗ, вам понадобится электронная подпись. Заказать её вы можете у нас. Мы принимаем заявки от юридических лиц, индивидуальных предпринимателей и физических лиц.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="products">
        <div class="container">
            <div class="row products_item_row">
                <div class="products_item">
                    <p class="title">Ускоренное получение электронной цифровой подписи</p>
                    <p class="title2">ПОДГОТОВКА ДОКУМЕНТОВ</p>
                    <p class="descr">Готовим пакет документов для заявки</p>
                    <p class="title2">ЗАКАЗ ЭЛЕКТРОННОЙ ПОДПИСИ</p>
                    <p class="descr">Заказываем электронную подпись в УЦ</p>
                    <p class="title2">ВЫДАЧА ЭЛЕКТРОННОЙ ПОДПИСИ В УЦ</p>
                    <p class="descr">Вы забираете носитель с электронной подписью в ближайшем удостоверяющем центре</p>
                    <div class="products_item_bottom">
                        <p class="price mt"><span>1500 <span class="sm">руб.</span></span></p>
                        <a class="btn btn-2">Выбрать</a>
                    </div>

                </div>
                <div class="products_item">
                    <p class="title">Ускоренное получение ЭЦП с доставкой в офис</p>
                    <p class="title2">ПОДГОТОВКА ДОКУМЕНТОВ</p>
                    <p class="descr">Готовим пакет документов для заявки</p>
                    <p class="title2">ЗАКАЗ ЭЛЕКТРОННОЙ ПОДПИСИ</p>
                    <p class="descr">Заказываем электронную подпись в УЦ</p>
                    <p class="title2">ДОСТАВЛЯЕМ ЭЦП К ВАМ</p>
                    <p class="descr">Доставляем носитель с электронной подписью к вам домой или в офис</p>
                    <div class="products_item_bottom">
                        <p class="price mt"><span>2600 <span class="sm">руб.</span></span></p>
                        <a class="btn btn-2">Выбрать</a>
                    </div>

                </div>
                <div class="products_item">
                    <p class="title">Ускоренное получение ЭЦП с доставкой в офис и настройкой компьютера</p>
                    <p class="title2">ПОДГОТОВКА ДОКУМЕНТОВ И ЗАКАЗ ЭЦП</p>
                    <p class="descr">Готовим пакет документов и заказываем электронную подпись</p>
                    <p class="title2">ДОСТАВЛЯЕМ ЭЦП К ВАМ</p>
                    <p class="descr">Доставляем носитель с электронной подписью домой или в офис</p>
                    <p class="title2">НАСТРОЙКА КОМПЬТЕРА ДЛЯ РАБОТЫ С ЭЦП</p>
                    <p class="descr">Настраиваем ваш компьютер для работы с ЭЦП по удалённому доступу</p>
                    <div class="products_item_bottom">
                        <p class="price old mt"><span>3000 <span class="sm">руб.</span> </span></p>
                        <p class="price"> <span>2600 <span class="sm">руб.</span></span></p>
                        <a class="btn btn-2">Выбрать</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--div class="reviews row">
        <div class="col-sm-6 img-people">
            &nbsp;
        </div>
        <div class="col-sm-6  article dark">
            <div class="js-slider reviews-slider">
                <div class="reviews_item">
                    <div class="quote quote_offset">
                        <p class="reviews_name"><span>Константинопольский</span> Константин Константинович</p>
                        <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                        <p class="reviews_text">
                            Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                            Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                        </p>
                    </div>
                    <div class="quote_offset">
                        <a class="btn">Все отзывы</a>
                    </div>
                </div>
                <div class="reviews_item ">
                    <div class="quote quote_offset">
                        <p class="reviews_name"><span>Second</span> Константин Константинович</p>
                        <p class="reviews_work">главный Заместитель замещающего заместителя</p>
                        <p class="reviews_text">
                            Наша компания впервые обратилась в маркетинговое агентство "Жуков и Архангельский" около полугода назад с задачей разработать продающий сайт, который реально генерирует продажи, а не просто висит мертвым грузом, проедая бюджет.
                            Результаты превзошли ожидания: рост продаж за первый квартал составил +96%, и это в “мертвый сезон”, когда обычно у нас убыток. Планируем заказать еще несколько сайтов.
                        </p>
                    </div>
                    <div class="quote_offset">
                        <a class="btn">Все отзывы</a>
                    </div>
                </div>

            </div>
        </div>
    </div-->

    <div class="hover-grey-blocks">
        <div class="container3">
            <div class="row hover-grey-box not-fix fix-sm">
                <div class="col-sm-4">
                    <div class="content ">
                        <div class="head row">
                            <p class="name">Покупателю</p>
                        </div>
                        <div class="text">
                            Поможем принять участие в закупке товаров или услуг для государственных и негосударственных организаций
                        </div>
                        <p><a href="" class="arrow">Перейти</a></p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <p class="name">Организатору торгов</p>
                        </div>
                        <div class="text">
                            Поможем найти лучшего поставщика, исходя из ваших требований по цене и качеству
                        </div>
                        <p><a href="" class="arrow">Перейти</a></p>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="content">
                        <div class="head row">
                            <p class="name">Закупки по 223 ФЗ</p>
                        </div>
                        <div class="text">
                            Поможем с подготовкой ТЗ и документации, проследим за соблюдением законодательства, поможем отбиться от жалоб и претензий.
                        </div>
                        <p><a href="" class="arrow">Перейти</a></p>

                    </div>

                </div>

            </div>
        </div>
    </div>

<!--    <div class="colors-box-block">-->
<!--        <div class="">-->
<!--            <div class="row">-->
<!--                    <div class="col-sm-3 item">-->
<!--                        <p class="title">Покупателю</p>-->
<!--                        <p class="descr">Поможем принять участие в закупке товаров или услуг для государственных и негосударственных организаций</p>-->
<!--                    </div>-->
<!--                    <div class="col-sm-3 item">-->
<!--                        <p class="title">Организатору торгов</p>-->
<!--                        <p class="descr">Поможем найти лучшего поставщика, исходя из ваших требований по цене и качеству</p>-->
<!--                    </div>-->
<!--                    <div class="col-sm-3 item">-->
<!--                        <p class="title">Закупки по 223 ФЗ</p>-->
<!--                        <p class="descr">Поможем с подготовкой ТЗ и документации, проследим за соблюдением законодательства, поможем отбиться от жалоб и претензий.</p>-->
<!--                    </div>-->
<!--                    <div class="col-sm-3 item">-->
<!--                        <p class="title">Все услуги</p>-->
<!--                        <p class="descr">Мы оказываем множество услуг для поставщиков и организаторов закупок. Выберите услугу, которая интересует вас.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <div class="form_block">
        <div class="container">
            <div class="col-sm-8">
                <h2 class="underline">Пишите нам</h2>
                <form>
                    <p>Наш менеджер свяжется с вами в течение рабочего дня и обязательно поможет</p>
                    <div class="col-sm-12 control"><input type="text" placeholder="Имя*"></div>
                    <div class="col-sm-6 control"><div class="offset"><input type="email" placeholder="E-mail*"></div></div>
                    <div class="col-sm-6 control"><div class="offset"><input type="text" placeholder="Телефон*"></div></div>
                    <div class="col-sm-12 control"><textarea placeholder="Ваш вопрос"></textarea></div>
                    <div class="col-sm-8 control left">
                        <label class="row">
                            <div class="checkbox_input"><input type="checkbox" class="regular-checkbox" checked></div>
                            <p class="checkbox_txt">Я согласен с  условиями обработки персональных данных</p>
                        </label>
                    </div>
                    <div class="col-sm-4 control right">
                        <button class="btn">Перезвоните мне</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <h2 class="underline">Cвязаться</h2>
                <p>АО «Центр дистанционных торгов»</p>
                <div class="contacts_ico_block">
                    <div>
                        <i class="ico ico-map"></i>
                        <p>191028 г. Санкт-Петербург, ул. Чайковского, д.12</p>
                    </div>
                    <div>
                        <i class="ico ico-mail"></i>
                        <p>help@cdtrf.ru</p>
                    </div>
                    <div>
                        <i class="ico ico-phone"></i>
                        <p>+7 (812) 64-64-818 <br>+7 (812) 64-64-810</p>
                    </div>
                    <div>
                        <i class="ico ico-site"></i>
                        <p>www.cdtrf.ru</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


<?php include("part/footer.php")?>

<script src="../public/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="../public/js/slick.min.js" type="text/javascript"></script>
<script src="../public/js/main.js?01" type="text/javascript"></script>

<script>
    $('.menu-catalog').addClass('active');
</script>

<script src="../public/js/Chart.min.js"></script>
<script src="../public/js/myChart.js"></script>

</body>

</html>
